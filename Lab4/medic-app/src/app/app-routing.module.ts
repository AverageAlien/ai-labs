import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { SickComponent } from './components/sick/sick.component';
import { InfoComponent } from './components/info/info.component';


const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'sicks', component: SickComponent },
  { path: 'info', component: InfoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
