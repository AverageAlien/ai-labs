import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SickServiceService {
  symptomMatrix = [
    // top - illnesses, left - symptoms
    [20, 20, 20, 20, 15, 30, 20, 0],
    [10, 10, 0, 0, 0, 0, 0, 0],
    [10, 10, 20, 0, 0, 0, 0, 0],
    [0, 0, 0, 20, 15, 0, 0, 0],
    [0, 0, 0, 30, 10, 30, 0, 0],
    [0, 0, 30, 0, 0, 0, 0, 0],
    [30, 0, 0, 0, 30, 0, 0, 0],
    [0, 0, 0, 0, 30, 0, 0, 0],
    [0, 0, 0, 0, 0, 40, 0, 0],
    [0, 0, 0, 0, 0, 0, 40, 40],
    [0, 0, 0, 0, 0, 0, 40, 0],
    [0, 0, 0, 0, 0, 0, 0, 60],
    [0, 30, 15, 15, 0, 0, 0, 0],
    [0, 30, 15, 15, 0, 0, 0, 0],
    [30, 0, 0, 0, 0, 0, 0, 0]
  ];

  diseases = [
    'Кір',
    'Грип',
    'Запалення легень',
    'Ангіна',
    'Скарлатина',
    'Свинка',
    'Дизентерія',
    'Гепатит'
  ];

  symptoms = [
    'Температура',
    'Нежить',
    'Кашель',
    'Біль горла',
    'Збільшення підчелюсних залоз',
    'Віддих',
    'Різна  висипка',
    'Тошнота рвота',
    'Опухлість завушних залоз',
    'Болі в животі',
    'Розлади кишківника',
    'Пожовтіння шкіри',
    'Головний біль',
    'Озноб',
    'Світлобоязнь'
  ];

  activatedSymptoms: boolean[];

  calcDisease(symptoms: number[], disease: number): number {
    const sum = symptoms
      .map(s => this.symptomMatrix[s][disease])
      .reduce((a, b) => a + b, 0);

    return sum;
  }

  getSymptoms(disease: number): {sympName: string, influence: number}[] {
    const diseaseSymptoms = this.symptomMatrix.map((v, i) => ({sympName: this.symptoms[i], influence: v[disease]}))
      .filter(v => v.influence > 0);

    return diseaseSymptoms;
  }

  constructor() {
    this.activatedSymptoms = new Array<boolean>(this.symptomMatrix.length);
    this.activatedSymptoms.fill(false);
  }
}
