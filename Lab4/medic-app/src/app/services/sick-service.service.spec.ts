import { TestBed } from '@angular/core/testing';

import { SickServiceService } from './sick-service.service';

describe('SickServiceService', () => {
  let service: SickServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SickServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
