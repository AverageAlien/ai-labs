import { Component, OnInit } from '@angular/core';
import { SickServiceService } from 'src/app/services/sick-service.service';

@Component({
  selector: 'app-sick',
  templateUrl: './sick.component.html',
  styleUrls: ['./sick.component.scss']
})
export class SickComponent implements OnInit {
  activeSymptoms: string[];
  selected: number = null;
  resultChance: number = null;

  constructor(public sickSvc: SickServiceService) { }

  ngOnInit(): void {
    this.activeSymptoms = this.sickSvc.activatedSymptoms
      .map((v, i) => v ? this.sickSvc.symptoms[i] : '-')
      .filter(v => v !== '-');
  }

  onSelectDisease(disIndex: number) {
    this.resultChance = this.sickSvc.calcDisease(
      this.sickSvc.activatedSymptoms.map((v, i) => v ? i : -1)
        .filter(v => v >= 0), disIndex
    );
    this.selected = disIndex;
  }

  onResetSelection() {
    this.selected = null;
    this.resultChance = null;
  }

  checkIfHasSymptom(symp: {sympName: string, influence: number}): boolean {
    return this.activeSymptoms.indexOf(symp.sympName) >= 0;
  }

}
