import { Component, OnInit } from '@angular/core';
import { SickServiceService } from 'src/app/services/sick-service.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  constructor(public sickSvc: SickServiceService) { }

  ngOnInit(): void {
  }

}
