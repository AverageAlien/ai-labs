import { Component, OnInit } from '@angular/core';
import { SickServiceService } from 'src/app/services/sick-service.service';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  symptomFields: string[];

  constructor(public sickSvc: SickServiceService) { }

  ngOnInit(): void {
    this.symptomFields = this.sickSvc.symptoms;
  }

}
