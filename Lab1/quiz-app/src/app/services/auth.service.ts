import { Injectable } from '@angular/core';
import { User } from '../interfaces/user.interface';

const Accounts: User[] = [
  {
    username: 'DefaultUser',
    password: '1234'
  },
  {
    username: 'Mike',
    password: 'mike'
  },
  {
    username: 'BudnykR',
    password: '12345'
  }
];

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUser: User;
  get currentUsername(): string {
    if (this.currentUser) {
      return this.currentUser.username;
    }
    return null;
  }

  constructor() { }

  logIn(credentials: User): boolean {
    const valid = !!Accounts.find(c => c.username === credentials.username && c.password === credentials.password);
    if (valid) {
      this.currentUser = credentials;
    }
    return valid;
  }
}
