import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { MenuComponent } from './components/menu/menu.component';
import { QuizComponent } from './components/quiz/quiz.component';
import { AuthGuard } from './routerGuards/auth.guard';
import { ResultComponent } from './components/result/result.component';
import { SummaryComponent } from './components/summary/summary.component';
import { Lab2mainComponent } from './components/lab2main/lab2main.component';
import { Lab3mainComponent } from './components/lab3main/lab3main.component';


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'menu', component: MenuComponent, canActivate: [AuthGuard] },
  { path: 'quiz', component: QuizComponent, canActivate: [AuthGuard] },
  { path: 'result', component: ResultComponent, canActivate: [AuthGuard] },
  { path: 'summary', component: SummaryComponent, canActivate: [AuthGuard] },
  { path: 'lab2', component: Lab2mainComponent },
  { path: 'lab3', component: Lab3mainComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
