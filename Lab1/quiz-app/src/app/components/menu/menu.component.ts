import { Component, OnInit } from '@angular/core';
import { AvailableQuizzes, QuizService } from 'src/app/services/quiz.service';
import { Quiz } from 'src/app/interfaces/quiz.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {
  public LoadedQuizzes: Quiz[];

  constructor(private quizService: QuizService, private router: Router) {
    this.LoadedQuizzes = AvailableQuizzes;
  }

  onQuizSelected(quizIndex: number) {
    this.quizService.loadQuiz(this.LoadedQuizzes[quizIndex]);
    this.router.navigate(['/quiz']);
  }

}
