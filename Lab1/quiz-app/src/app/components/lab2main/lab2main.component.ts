import { Component, OnInit } from '@angular/core';
import { Lab2svcService } from 'src/app/services/lab2svc.service';
import { FormGroup, FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-lab2main',
  templateUrl: './lab2main.component.html',
  styleUrls: ['./lab2main.component.scss']
})
export class Lab2mainComponent implements OnInit {
  arrayInputs: FormGroup;
  matrixInputs: FormGroup;
  matrixData: any[] = [];
  matrixDataSource = new MatTableDataSource<any>(this.matrixData);

  graph = {
    data: [
        { x: [], y: [], type: 'scatter', mode: 'lines+points', marker: {color: 'red'} }
    ],
    layout: {width: 512, height: 350, title: 'Графік нечіткої множини'}
  };

  displayedColumns = ['temp', 'val1', 'val2', 'val3', 'val4', 'val5', 'val6'];
  matrix: number[][];

  constructor(public lab2svc: Lab2svcService) { }

  ngOnInit(): void {
    this.arrayInputs = new FormGroup({
      val1: new FormControl(null),
      val2: new FormControl(null),
      val3: new FormControl(null),
      val4: new FormControl(null),
      val5: new FormControl(null),
      val6: new FormControl(null),
    });
    this.matrixInputs = new FormGroup({
      val1: new FormControl(null),
      val2: new FormControl(null),
      val3: new FormControl(null),
      val4: new FormControl(null),
      val5: new FormControl(null),
      val6: new FormControl(null),
    });
  }

  public submitArray() {
    this.lab2svc.valueArray = [];
    // tslint:disable-next-line: prefer-const
    // tslint:disable-next-line: forin
    for (const n in this.arrayInputs.value) {
      this.lab2svc.valueArray.push(Number(this.arrayInputs.value[n]));
    }
    this.lab2svc.state = 1;
  }

  public calculateMatrix() {
    this.matrixData = [];

    this.matrix = new Array<number[]>(6);
    for (let i = 0; i < 6; ++i) {
      this.matrix[i] = new Array<number>(6);
    }

    for (let i = 0; i < 6; ++i) {
      this.matrix[i][i] = 1;
    }

    const inputNumbers = [];
    // tslint:disable-next-line: forin
    for (const n in this.matrixInputs.value) {
      inputNumbers.push(Number(this.matrixInputs.value[n]));
    }

    for (let i = 0; i < 5; ++i) {
      this.matrix[5][i] = inputNumbers[i];
    }

    for (let i = 4; i > 0; --i) {
      for (let j = 0; j < i; ++j) {
        this.matrix[i][j] = this.matrix[i + 1][j] / this.matrix[i + 1][i];
      }
    }

    for (let i = 0; i < 5; ++i) {
      for (let j = i + 1; j < 6; ++j) {
        this.matrix[i][j] = 1 / this.matrix[j][i];
      }
    }

    this.matrix.forEach((row, i) => {
      this.matrixData.push({
        temp: this.lab2svc.valueArray[i],
        val1: row[0].toFixed(2),
        val2: row[1].toFixed(2),
        val3: row[2].toFixed(2),
        val4: row[3].toFixed(2),
        val5: row[4].toFixed(2),
        val6: row[5].toFixed(2)
      });
    });

    const cSums: number[] = [];

    for (let i = 0; i < 6; ++i) {
      cSums.push(this.colSum(i));
    }

    this.matrixData.push({
      temp: 1,
      val1: cSums[0].toFixed(2),
      val2: cSums[1].toFixed(2),
      val3: cSums[2].toFixed(2),
      val4: cSums[3].toFixed(2),
      val5: cSums[4].toFixed(2),
      val6: cSums[5].toFixed(2),
    });

    this.matrixData.push({
      temp: 2,
      val1: (1 / cSums[0]).toFixed(2),
      val2: (1 / cSums[1]).toFixed(2),
      val3: (1 / cSums[2]).toFixed(2),
      val4: (1 / cSums[3]).toFixed(2),
      val5: (1 / cSums[4]).toFixed(2),
      val6: (1 / cSums[5]).toFixed(2),
    });

    const maxSum = Math.max(...cSums.map(c => 1 / c));

    this.matrixData.push({
      temp: 'M(X)',
      val1: (1 / cSums[0] / maxSum).toFixed(2),
      val2: (1 / cSums[1] / maxSum).toFixed(2),
      val3: (1 / cSums[2] / maxSum).toFixed(2),
      val4: (1 / cSums[3] / maxSum).toFixed(2),
      val5: (1 / cSums[4] / maxSum).toFixed(2),
      val6: (1 / cSums[5] / maxSum).toFixed(2),
    });

    console.log(this.matrixData);
    this.matrixDataSource = new MatTableDataSource<any>(this.matrixData);

    this.lab2svc.state = 2;

    for (let i = 0; i < 6; ++i) {
      this.graph.data[0].x.push(this.lab2svc.valueArray[i]);
      this.graph.data[0].y.push(1 / cSums[i] / maxSum);
    }


  }

  private colSum(col: number): number {
    let sum = 0;
    for (let i = 0; i < 6; ++i) {
      sum += this.matrix[i][col];
    }
    return sum;
  }

}
