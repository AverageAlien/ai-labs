import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/interfaces/user.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  error: string = null;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl(null),
      password: new FormControl(null)
    });
  }

  onLoginSubmit() {
    const credentials: User = {
      username: this.loginForm.value.username,
      password: this.loginForm.value.password
    };

    if (this.authService.logIn(credentials)) {
      this.router.navigate(['/menu']);
    } else {
      this.error = 'Invalid login.';
    }
  }

}
