import { Component, AfterViewInit, OnInit } from '@angular/core';
import { QuizService, AvailableQuizzes } from 'src/app/services/quiz.service';
import { Quiz } from 'src/app/interfaces/quiz.interface';
import * as CanvasJS from '../../../assets/canvasjs.min';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements AfterViewInit, OnInit {
  public yourLevel: Quiz;
  get topQuiz(): Quiz {
    const completedQuizzes = AvailableQuizzes.filter(q => q.completed);
    console.log(completedQuizzes.length);
    return completedQuizzes.reduce((a, b) =>
      ((this.quizService.calcQuizScore(a) > this.quizService.calcQuizScore(b)) ?
      a : b), completedQuizzes[0]);
  }

  constructor(public quizService: QuizService) { }

  ngOnInit(): void {
    this.yourLevel = this.topQuiz;
  }

  ngAfterViewInit(): void {
    const dataGroups = [];
    const completedQuizzes = AvailableQuizzes.filter(q => q.completed);
    const maxQuestions = Math.max(...completedQuizzes.map(q => q.questions.length));
    const colors = ['#5ac600', '#7b91ae', '#fd8b84', '#e8971c'];

    for (let i = 0; i < maxQuestions; ++i) {
      dataGroups.push({
        type: 'stackedColumn',
        showInLegend: true,
        color: colors[i],
        name: `Питання №${i + 1}`,
        dataPoints: completedQuizzes.map(q => {
          return {
            y: (i < q.questions.length) ? q.questions[i].answerScores[q.selectedAnswers[i]] : 0,
            label: q.quizName
          };
        }),
      });
    }

    let chart = new CanvasJS.Chart('summaryChart', {
      animationEnabled: true,
      title: {
        text: 'Підсумки',
        fontFamily: 'arial black',
        fontColor: '#695A42'
      },
      toolTip: {
        shared: true,
        content: this.toolTipContent
      },
      data: dataGroups,
    });
    chart.render();
  }

  private toolTipContent(e) {
    let str = '';
    let total = 0;
    let str2: string;
    let str3: string;
    for (let i = 0; i < e.entries.length; i++) {
      const  str1 = '<span style= "color:' +
        e.entries[i].dataSeries.color + '"> ' +
        e.entries[i].dataSeries.name +
        '</span>: <strong>' +
        e.entries[i].dataPoint.y +
        '</strong><br/>';
      total = e.entries[i].dataPoint.y + total;
      str = str.concat(str1);
    }
    str2 = '<span style = "color:DodgerBlue;"><strong>' + (e.entries[0].dataPoint.label) + '</strong></span><br/>';
    total = Math.round(total * 100) / 100;
    str3 = '<span style = "color:Tomato">Всього:</span><strong> ' + total + '</strong><br/>';
    return (str2.concat(str)).concat(str3);
  }

}
