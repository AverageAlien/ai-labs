import { Component, OnInit, AfterViewInit } from '@angular/core';
import { QuizService } from 'src/app/services/quiz.service';
import * as CanvasJS from '../../../assets/canvasjs.min';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements AfterViewInit {

  constructor(public quizService: QuizService) { }

  ngAfterViewInit(): void {
    const chart = new CanvasJS.Chart('resultChart', {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: 'Статистика'
      },
      data: [{
        type: 'column',
        dataPoints: this.quizService.currentQuiz.questions.map((x, i) => {
          return {
            y: x.answerScores[this.quizService.currentQuiz.selectedAnswers[i]],
            label: `Питання ${i + 1}`
          };
        })
      }]
    });

    chart.render();
  }

}
