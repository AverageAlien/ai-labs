import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-lab3main',
  templateUrl: './lab3main.component.html',
  styleUrls: ['./lab3main.component.scss']
})
export class Lab3mainComponent implements OnInit {
  resultMatrix: any[] = null;
  resultMatrixDataSource: MatTableDataSource<any[]>;

  profTable = [
    ['Швидкість та гнучкість мислення',
    0.9, 0.8, 0.3, 0.5, 0.7],
    ['Вміння швидко приймати рішення',
    0.9, 0.5, 0.9, 0.4, 0.8],
    ['Стійкість та концентрація уваги',
    0.8, 0.9, 0.6, 0.5, 0.8],
    ['Зорова пам\'ять',
    0.4, 0.3, 0.5, 0.5, 0.2],
    ['Швидкість реакції',
    0.5, 0.1, 0.9, 0.2, 0.6],
    ['Рухлива пам\'ять',
    0.3, 0.2, 0.8, 0.2, 0.2],
    ['Фізична виносливість',
    0.6, 0.2, 0.9, 0.3, 0.2],
    ['Координація рухів',
    0.2, 0.2, 0.8, 0.3, 0.3],
    ['Емоційно-вольова стійкість',
    0.9, 0.5, 0.6, 0.9, 0.3],
    ['Відповідальність',
    0.8, 0.5, 0.3, 0.8, 0.2]
  ];
  jobs = [
    'Менеджер',
    'Програміст',
    'Шофер',
    'Референт',
    'Перекладач'
  ];
  selectedCand = 'Кандидат 1';
  cands = [
    'Кандидат 1',
    'Кандидат 2',
    'Кандидат 3'
  ];
  profMatrix = [
    [0.9, 0.8, 0.3, 0.5, 0.7],
    [0.9, 0.5, 0.9, 0.4, 0.8],
    [0.8, 0.9, 0.6, 0.5, 0.8],
    [0.4, 0.3, 0.5, 0.5, 0.2],
    [0.5, 0.1, 0.9, 0.2, 0.6],
    [0.3, 0.2, 0.8, 0.2, 0.2],
    [0.6, 0.2, 0.9, 0.3, 0.2],
    [0.2, 0.2, 0.8, 0.3, 0.3],
    [0.9, 0.5, 0.6, 0.9, 0.3],
    [0.8, 0.5, 0.3, 0.8, 0.2]
  ];

  inputMatrix: number[][] = null;


  graph = {
    data: [
        { x: [], y: [], name: 'Кандидат 1', type: 'bar', marker: {color: 'blue'} },
        { x: [], y: [], name: 'Кандидат 2', type: 'bar', marker: {color: 'red'} },
        { x: [], y: [], name: 'Кандидат 3', type: 'bar', marker: {color: 'orange'} },
    ],
    layout: {width: 512, height: 350, title: 'Результати кандидата'}
  };

  skillInputs: FormGroup;

  columns = [
    'skill',
    'val1',
    'val2',
    'val3',
    'val4',
    'val5'
  ];
  resultCols = [
    'skill',
    'cand1',
    'cand2',
    'cand3'
  ];

  constructor() { }

  ngOnInit(): void {
    this.skillInputs = new FormGroup({
      v0: new FormControl(0.5),
      v1: new FormControl(0.5),
      v2: new FormControl(0.5),
      v3: new FormControl(0.5),
      v4: new FormControl(0.5),
      v5: new FormControl(0.5),
      v6: new FormControl(0.5),
      v7: new FormControl(0.5),
      v8: new FormControl(0.5),
      v9: new FormControl(0.5)
    });

    this.inputMatrix = new Array<number[]>(this.cands.length);
    this.inputMatrix[0] = [];
    for (let i = 0; i < 10; ++i) {
      this.inputMatrix[0].push(0.5);
    }

    const inps = Object.keys(this.skillInputs.value);
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < this.inputMatrix[0].length; ++i) {
      this.skillInputs.value[inps[i]] = this.inputMatrix[0][i];
    }
  }



  onCalculate() {
    const skillMatrix: number[][] = new Array<number[]>(this.inputMatrix.length);

    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < skillMatrix.length; ++i) {
      skillMatrix[i] = [];
      // tslint:disable-next-line: prefer-for-of
      for (let j = 0; j < this.inputMatrix[i].length; ++j) {
        skillMatrix[i].push(this.inputMatrix[i][j]);
      }
    }
    // console.log(skillMatrix);

    const calcMatrix = this.maxmin(skillMatrix, this.profMatrix);

    this.resultMatrix = [];
    for (let i = 0; i < 3; ++i) {
      this.graph.data[i].x = [];
      this.graph.data[i].y = [];
    }

    for (let i = 0; i < calcMatrix[0].length; ++i) {
      const temp = [];
      temp.push(this.jobs[i]);
      for (let j = 0; j < calcMatrix.length; ++j) {
        temp.push(calcMatrix[j][i]);
        this.graph.data[j].x = this.jobs;
        this.graph.data[j].y.push(calcMatrix[j][i]);
      }
      this.resultMatrix.push(temp);
    }
    console.log(this.graph);

    // console.log(this.resultMatrix);
    this.resultMatrixDataSource = new MatTableDataSource<any[]>(this.resultMatrix);
  }

  maxmin(A: number[][], B: number[][]): number[][] {
    console.log(A);
    console.log(B);
    const result: number[][] = [];
    for (let i = 0; i < A.length; ++i) {
      result.push(new Array<number>(B[0].length));
      for (let j = 0; j < B[0].length; ++j) {
        const cellBuffer: number[] = [];
        for (let k = 0; k < A[i].length; ++k) {
          cellBuffer.push(Math.min(A[i][k], B[k][j]));
        }
        result[i][j] = Math.max(...cellBuffer);
      }
    }
    console.log(result);
    return result;
  }

  onSliderChange() {
    const candNum = this.cands.indexOf(this.selectedCand);
    this.inputMatrix[candNum] = [];
    // tslint:disable-next-line: forin
    for (const slider in this.skillInputs.value) {
      this.inputMatrix[candNum].push(this.skillInputs.value[slider]);
    }
  }

  onCandChange() {
    const candNum = this.cands.indexOf(this.selectedCand);
    if (!this.inputMatrix[candNum]) {
      this.inputMatrix[candNum] = [];
      for (let i = 0; i < 10; ++i) {
        this.inputMatrix[candNum].push(0.5);
      }
    }

    const inps = Object.keys(this.skillInputs.value);
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < this.inputMatrix[candNum].length; ++i) {
      this.skillInputs.value[inps[i]] = this.inputMatrix[candNum][i];
    }
  }
}
