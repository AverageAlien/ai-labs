import { Component, OnInit } from '@angular/core';
import { QuizService, AvailableQuizzes } from 'src/app/services/quiz.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {
  get questionNumber(): number {
    return this.quizService.currentQuestion;
  }
  get currentQuestion(): string {
    return this.quizService.currentQuiz.questions[this.quizService.currentQuestion].caption;
  }
  get currentAnswers(): string[] {
    return this.quizService.currentQuiz.questions[this.quizService.currentQuestion].answers;
  }
  get currentIsAnswered(): boolean {
    return this.quizService.currentQuiz.selectedAnswers[this.questionNumber] !== undefined;
  }

  constructor(public quizService: QuizService, private router: Router) { }

  ngOnInit(): void {
    // this.quizService.loadQuiz(AvailableQuizzes[0]);
  }

  onAnswerClick(answer: number): void {
    this.quizService.currentQuiz.selectedAnswers[this.questionNumber] = answer;
  }

  onViewResults(): void {
    this.quizService.currentQuiz.completed = true;
    this.router.navigate(['/result']);
  }

}
