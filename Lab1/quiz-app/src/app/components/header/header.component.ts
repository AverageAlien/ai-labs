import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { QuizService, AvailableQuizzes } from 'src/app/services/quiz.service';
import { Quiz } from 'src/app/interfaces/quiz.interface';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  loadedQuizzes: Quiz[];

  constructor(public authService: AuthService, public quizService: QuizService) { }

  ngOnInit(): void {
    this.loadedQuizzes = AvailableQuizzes;
  }

  checkIfAnyCompleted(): boolean {
    return AvailableQuizzes.some(q => q.completed);
  }

}
