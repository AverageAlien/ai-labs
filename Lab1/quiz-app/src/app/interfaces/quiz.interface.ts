import { Question } from './question.interface';

export interface Quiz {
  quizName: string;
  quizDesc: string;
  questions: Question[];
  selectedAnswers: number[];
  completed: boolean;
}
