export interface Question {
  caption: string;
  answers: string[];
  answerScores: number[];
}
